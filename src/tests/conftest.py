import pytest
from starlette.testclient import TestClient

from app.main import app_server


@pytest.fixture(scope="module")
def test_app():
    client = TestClient(app_server)
    yield client
