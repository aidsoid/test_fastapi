import asyncio
import aiokafka


class KafkaManager():
    KAFKA_BOOTSTRAP_SERVERS = "localhost:9093"
    KAFKA_TOPIC = "kafka"
    KAFKA_CONSUMER_GROUP_ID = "group-id"

    def __init__(self):
        self.consumer = None
        self.producer = None

    async def consumer_start(self):
        loop = asyncio.get_running_loop()
        self.consumer = aiokafka.AIOKafkaConsumer(
            self.KAFKA_TOPIC,
            loop=loop,
            bootstrap_servers=self.KAFKA_BOOTSTRAP_SERVERS,
            group_id=self.KAFKA_CONSUMER_GROUP_ID
        )
        await self.consumer.start()

    async def producer_start(self):
        loop = asyncio.get_running_loop()
        self.producer = aiokafka.AIOKafkaProducer( loop=loop, bootstrap_servers=self.KAFKA_BOOTSTRAP_SERVERS)
        await self.producer.start()

    async def consumer_stop(self):
        self.consumer.stop()

    async def producer_stop(self):
        await self.producer.stop()


kafka_manager = KafkaManager()