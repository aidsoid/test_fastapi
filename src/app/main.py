import uvicorn
from fastapi import FastAPI

from config import settings
from routes import router
from kafka_manager import kafka_manager


def get_application() -> FastAPI:
    app = FastAPI(
        title='Chat with websockets',
        # on_startup=[
        #     kafka_manager.consumer_start,
        #     kafka_manager.producer_start
        # ],
        # on_shutdown=[
        #     kafka_manager.consumer_stop,
        #     kafka_manager.producer_stop
        # ],

    )
    app.include_router(router)
    return app


app_server = get_application()

if __name__ == '__main__':
    uvicorn.run(app_server, host=settings.host, port=settings.port)
